package uz.book.bookapp.service;

import uz.book.bookapp.entity.user.UserActivity;

public interface CartService {

    boolean buyAll(UserActivity userActivity);
}
