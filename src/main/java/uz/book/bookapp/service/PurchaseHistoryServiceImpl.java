package uz.book.bookapp.service;


//Asadbek Xalimjonov 29/03/22 00:50

import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.book.bookapp.projection.CustomPurchaseHistory;
import uz.book.bookapp.repository.PurchaseHistoryRepo;

import java.util.List;

@Service
public class PurchaseHistoryServiceImpl implements PurchaseHistoryService {


    @Autowired
    private PurchaseHistoryRepo purchaseHistoryRepo;

    @Override
    public List<CustomPurchaseHistory> getPurchaseHistory(String userId) {
        return purchaseHistoryRepo.getPurchaseHistory(userId);
    }
}
