package uz.book.bookapp.service;

import uz.book.bookapp.projection.CustomPurchaseHistory;

import java.util.List;

public interface PurchaseHistoryService {

    List<CustomPurchaseHistory> getPurchaseHistory(String userId);

}
