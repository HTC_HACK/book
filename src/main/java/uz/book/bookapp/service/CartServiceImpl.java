package uz.book.bookapp.service;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import uz.book.bookapp.projection.CustomCart;
import uz.book.bookapp.entity.payment.PurchaseHistory;
import uz.book.bookapp.entity.user.User;
import uz.book.bookapp.entity.user.UserActivity;
import uz.book.bookapp.repository.BookRepo;
import uz.book.bookapp.repository.CartRepo;
import uz.book.bookapp.repository.PurchaseHistoryRepo;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 28/03/22 23:35

@Service
public class CartServiceImpl implements CartService {

    @Autowired
    private CartRepo cartRepo;

    @Autowired
    private PurchaseHistoryRepo purchaseHistoryRepo;

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private BookRepo bookRepo;

    @Autowired
    public CartServiceImpl(CartRepo cartRepo) {
        this.cartRepo = cartRepo;
    }


    @Override
    public boolean buyAll(UserActivity userActivity) {

        try {

            User byIdChatId = userService.findByIdChatId(userActivity.getChatId());
            List<CustomCart> customCarts = userService.showMyCart(userActivity);
            List<PurchaseHistory> purchaseHistoryList = new ArrayList<>();

            Double sum = customCarts.stream()
                    .map(item -> item.getPrice() * item.getQuantity())
                    .reduce(0.0, (a, b) -> a + b);

            double total = sum * (1 + userActivity.getPayType().getCommissionFee() / 100);
            if ( total<= byIdChatId.getFullName().getBalance()) {
                for (CustomCart customCart : customCarts) {
                    if (bookRepo.findById(customCart.getBookId()).isPresent()) {
                        PurchaseHistory purchaseHistory1 = new PurchaseHistory();
                        purchaseHistory1.setBook(bookRepo.findById(customCart.getBookId()).get());
                        purchaseHistory1.setPayType(userActivity.getPayType());
                        purchaseHistory1.setQuantity(customCart.getQuantity());
                        purchaseHistory1.setUser(userActivity.getUser());
                        purchaseHistory1.setTotalPriceWithCommissionFee(customCart.getPrice() * (1 + userActivity.getPayType().getCommissionFee() / 100));
                        purchaseHistoryList.add(purchaseHistory1);
                    }
                }

                userService.removeBalance(userActivity.getUser().getId(), (sum * (1 + userActivity.getPayType().getCommissionFee() / 100)));
                purchaseHistoryRepo.saveAll(purchaseHistoryList);
                userService.clearCart(userActivity.getUser().getId());
                return true;
            }
            return false;
        } catch (Exception e) {
            e.getMessage();
        }
        return false;
    }
}
