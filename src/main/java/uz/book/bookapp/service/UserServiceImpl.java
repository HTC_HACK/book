package uz.book.bookapp.service;


//Asadbek Xalimjonov 28/03/22 17:47


import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.objects.Contact;
import uz.book.bookapp.entity.payment.Cart;
import uz.book.bookapp.entity.user.User;
import uz.book.bookapp.entity.user.UserActivity;
import uz.book.bookapp.entity.user.UserDetails;
import uz.book.bookapp.projection.CustomCart;
import uz.book.bookapp.repository.CartRepo;
import uz.book.bookapp.repository.UserRepo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepo userRepo;
    private final CartRepo cartRepo;


    public UserServiceImpl(UserRepo userRepo, CartRepo cartRepo) {
        this.userRepo = userRepo;
        this.cartRepo = cartRepo;
    }

    public Map<Long, UserActivity> userActivityMap() {
        Map<Long, UserActivity> userActivityMap = new HashMap<>();
        for (User user : getAllUser()) {
            UserActivity userActivity = new UserActivity();
            userActivity.setUser(user);
            userActivityMap.put(user.getChatId(), userActivity);
        }
        return userActivityMap;
    }

    private List<User> getAllUser() {
        return userRepo.findAll();
    }

    @Override
    public User saveUser(Long chatId) {

        try {
            User user = new User();
            user.setChatId(chatId);
            return userRepo.save(user);
        } catch (Exception e) {
            e.getMessage();
        }

        return null;
    }

    @Override
    public User findByIdChatId(Long chatId) {

        User currentUser = userRepo.findAll().stream().filter(user -> user.getChatId().longValue() == chatId).findFirst().orElse(null);

        if (currentUser != null) return currentUser;

        return saveUser(chatId);

    }

    @Override
    public User updateUser(User currentUser, Contact contact) {

        String firstName = contact.getFirstName();
        String lastName = contact.getLastName();
        String phoneNumber = contact.getPhoneNumber();

        if (userRepo.findById(currentUser.getId()).isPresent()) {
            User user = userRepo.findById(currentUser.getId()).get();
            user.setFullName(new UserDetails(firstName, lastName));
            user.setPhoneNumber(phoneNumber);
            return userRepo.save(user);
        }

        return null;
    }

    @Override
    public boolean addToCart(UserActivity userActivity) {
        try {
            Cart cart = new Cart();
            cart.setUser(userActivity.getUser());
            cart.setBook(userActivity.getSelectedBook());
            cart.setQuantity(userActivity.getCurrentQuantity());
            cartRepo.save(cart);
            return true;
        } catch (Exception e) {
            e.getMessage();
        }
        return false;
    }

    @Override
    public List<CustomCart> showMyCart(UserActivity currentUserActivity) {

        return cartRepo.getMyCart(currentUserActivity.getUser().getId());
    }

    @Override
    public void addBalance(String userId, double amount) {

        if (userRepo.findById(userId).isPresent()) {

            User user = userRepo.findById(userId).get();
            user.getFullName().setBalance(user.getFullName().getBalance() + amount);
            userRepo.save(user);
        }


    }

    @Override
    public void removeBalance(String userId, double amount) {
        if (userRepo.findById(userId).isPresent()) {

            User user = userRepo.findById(userId).get();
            user.getFullName().setBalance(user.getFullName().getBalance() - amount);
            userRepo.save(user);
        }
    }

    @Override
    public void clearCart(String userId) {

        List<CustomCart> myCart = cartRepo.getMyCart(userId);

        for (CustomCart customCart : myCart) {
            try {

                cartRepo.deleteById(customCart.getCartId());
            } catch (Exception e) {
                e.getMessage();
            }
        }

    }

    @Override
    public void removeBook(UserActivity currentUserActivity, CustomCart customCart1) {
        try {
            cartRepo.deleteById(customCart1.getCartId());
        } catch (Exception e) {
            e.getMessage();
        }
    }


}
