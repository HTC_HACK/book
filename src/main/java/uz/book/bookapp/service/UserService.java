package uz.book.bookapp.service;



//Asadbek Xalimjonov 28/03/22 17:46

import org.telegram.telegrambots.meta.api.objects.Contact;
import uz.book.bookapp.projection.CustomCart;
import uz.book.bookapp.entity.user.User;
import uz.book.bookapp.entity.user.UserActivity;

import java.util.List;

public interface UserService {

    User saveUser(Long chatId);

    User findByIdChatId(Long chatId);

    User updateUser(User currentUser, Contact contact);

    boolean addToCart(UserActivity userActivity);

    List<CustomCart> showMyCart(UserActivity currentUserActivity);

    void addBalance(String userId,double amount);

    void removeBalance(String userId,double amount);

    void clearCart(String userId);

    void removeBook(UserActivity currentUserActivity, CustomCart customCart1);

}
