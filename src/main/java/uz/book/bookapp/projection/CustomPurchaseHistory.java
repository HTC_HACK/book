package uz.book.bookapp.projection;


//Asadbek Xalimjonov 29/03/22 00:39

public interface CustomPurchaseHistory {

    String getBookName();

    double getQuantity();

    double getPrice();

    String getFullName();

    String getPhoneNumber();

    String getPayType();

    String getCommissionFee();

    double getTotalPriceWithCommissionFee();

    String getTimeDate();

}
