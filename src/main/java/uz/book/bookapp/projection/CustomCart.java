package uz.book.bookapp.projection;



//Asadbek Xalimjonov 28/03/22 21:54

public interface CustomCart {

    String getBookId();
    String getBookName();
    Double getPrice();
    int getQuantity();
    String getCartId();

}
