package uz.book.bookapp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import uz.book.bookapp.entity.book.Author;
import uz.book.bookapp.entity.book.Book;
import uz.book.bookapp.entity.book.BookCategory;
import uz.book.bookapp.entity.book.StoredBook;
import uz.book.bookapp.entity.payment.PayType;
import uz.book.bookapp.entity.user.Role;
import uz.book.bookapp.entity.user.User;
import uz.book.bookapp.entity.user.UserDetails;
import uz.book.bookapp.repository.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Component
public class DataLoader implements CommandLineRunner {


    @Value("${spring.sql.init.mode}")
    String initMode;

    final PayTypeRepo payTypeRepo;
    final AuthorRepo authorRepo;
    final BookRepo bookRepo;
    final BookCategoryRepo bookCategoryRepo;
    final UserRepo userRepo;
    final StoredBookRepo storedBookRepo;


    public DataLoader(PayTypeRepo payTypeRepo, AuthorRepo authorRepo, BookRepo bookRepo, BookCategoryRepo bookCategoryRepo, UserRepo userRepo, StoredBookRepo storedBookRepo) {
        this.payTypeRepo = payTypeRepo;
        this.authorRepo = authorRepo;
        this.bookRepo = bookRepo;
        this.bookCategoryRepo = bookCategoryRepo;
        this.userRepo = userRepo;
        this.storedBookRepo = storedBookRepo;
    }

    @Override
    public void run(String... args) throws Exception {
        if (initMode.equals("always")) {


            //payments
            List<PayType> payTypes = new ArrayList<>(
                    Arrays.asList(
                            new PayType("CLICK", 0.5),
                            new PayType("PAYME", 1.0),
                            new PayType("APELSIN", 0.3)
                    )
            );

            payTypeRepo.saveAll(payTypes);

            //Book Categories
            BookCategory bookCategory1 = new BookCategory("Backend");
            BookCategory bookCategory2 = new BookCategory("Frontend");

            BookCategory savedCategory1 = bookCategoryRepo.save(bookCategory1);
            BookCategory savedCategory2 = bookCategoryRepo.save(bookCategory2);


            //Authors
            Author author1 = new Author(new UserDetails("Andrew", "Robertson"));
            Author author2 = new Author(new UserDetails("Don", "Karleone"));
            Author author3 = new Author(new UserDetails("John", "Smith"));
            Author author4 = new Author(new UserDetails("Smith", "Rowe"));

            Author savedAuthor1 = authorRepo.save(author1);
            Author savedAuthor2 = authorRepo.save(author2);
            Author savedAuthor3 = authorRepo.save(author3);
            Author savedAuthor4 = authorRepo.save(author4);

            Book book1 = new Book("C++",
                    "C++",
                    1500.0, savedCategory1,
                    new ArrayList<>(
                            Arrays.asList(savedAuthor1, savedAuthor3)
                    ));

            Book savedBook1 = bookRepo.save(book1);


            Book book2 = new Book("Java",
                    "Java",
                    2500.0, savedCategory1,
                    new ArrayList<>(
                            Arrays.asList(savedAuthor1, savedAuthor2)
                    ));
            Book savedBook2 = bookRepo.save(book2);

            Book book3 = new Book("NodeJs",
                    "NodeJs",
                    3500.0, savedCategory2,
                    new ArrayList<>(
                            Arrays.asList(savedAuthor3, savedAuthor4)
                    ));
            Book savedBook3 = bookRepo.save(book3);

            Book book4 = new Book("VueJs",
                    "VueJs",
                    5000.0, savedCategory2,
                    new ArrayList<>(
                            Arrays.asList(savedAuthor1, savedAuthor2)
                    ));

            Book savedBook4 = bookRepo.save(book4);


            //Books Stored
            StoredBook storedBook1 = new StoredBook(savedBook1, 5);
            StoredBook storedBook2 = new StoredBook(savedBook2, 6);
            StoredBook storedBook3 = new StoredBook(savedBook3, 7);
            StoredBook storedBook4 = new StoredBook(savedBook4, 8);
            List<StoredBook> storedBooks = new ArrayList<>(
                    Arrays.asList(storedBook1, storedBook2, storedBook3, storedBook4)
            );

            storedBookRepo.saveAll(storedBooks);

            //Users

            User admin = new User(
                    "+998-99-443-62-24",
                    "asadbekxalimjonov",
                    0L,
                    Role.ADMIN,
                    0,
                    new UserDetails("Asadbek", "Xalimjonov")
            );

            userRepo.save(admin);


        }
    }
}
