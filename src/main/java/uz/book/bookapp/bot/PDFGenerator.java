package uz.book.bookapp.bot;


//Asadbek Xalimjonov 29/03/22 00:38

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import uz.book.bookapp.entity.user.UserDetails;
import uz.book.bookapp.projection.CustomPurchaseHistory;
import uz.book.bookapp.repository.PurchaseHistoryRepo;

import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;


import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class PDFGenerator {

    @Autowired
    private PurchaseHistoryRepo purchaseHistoryRepo;

    public File getPurchaseHistory(UserDetails fullName, List<CustomPurchaseHistory> purchaseHistory) {

        File file = null;
        try (PdfWriter writer = new PdfWriter("src/main/resources/purchaseHistory.pdf")) {

            PdfDocument pdfDocument = new PdfDocument(writer);

            pdfDocument.setDefaultPageSize(PageSize.A3);
            pdfDocument.addNewPage();


            Document document = new Document(pdfDocument);
            Paragraph paragraph = new Paragraph(" Purchase History " + fullName.getFirstName() + " - DATE " + LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy , HH:mm:ss"))).setFontSize(15);

            document.add(paragraph);


            float[] pointColumn = {20F,40f, 40F, 40F, 40F, 40F, 40F, 40F, 40F,40F};
            Table table = new Table(pointColumn);

            table.setTextAlignment(TextAlignment.CENTER).setHorizontalAlignment(HorizontalAlignment.CENTER);
            table.addCell(new Cell().add("ID").setBackgroundColor(Color.LIGHT_GRAY));
            table.addCell(new Cell().add("FULLNAME").setBackgroundColor(Color.LIGHT_GRAY));
            table.addCell(new Cell().add("BOOKNAME").setBackgroundColor(Color.LIGHT_GRAY));
            table.addCell(new Cell().add("PRICE").setBackgroundColor(Color.LIGHT_GRAY));
            table.addCell(new Cell().add("QUANTITY").setBackgroundColor(Color.LIGHT_GRAY));
            table.addCell(new Cell().add("PHONE NUMBER").setBackgroundColor(Color.LIGHT_GRAY));
            table.addCell(new Cell().add("PAY TYPE").setBackgroundColor(Color.LIGHT_GRAY));
            table.addCell(new Cell().add("COMMISSION FEE").setBackgroundColor(Color.LIGHT_GRAY));
            table.addCell(new Cell().add("TOTAL PRICE").setBackgroundColor(Color.LIGHT_GRAY));
            table.addCell(new Cell().add("DATE").setBackgroundColor(Color.LIGHT_GRAY));



            int i = 1;
            for (CustomPurchaseHistory orderItem : purchaseHistory) {
                if (i % 2 == 0) {
                    table.addCell(new Cell().add("" + (i++)).setBackgroundColor(Color.LIGHT_GRAY));
                    table.addCell(new Cell().add(orderItem.getFullName()).setBackgroundColor(Color.LIGHT_GRAY));
                    table.addCell(new Cell().add(orderItem.getBookName()).setBackgroundColor(Color.LIGHT_GRAY));
                    table.addCell(new Cell().add("" + orderItem.getPrice()).setBackgroundColor(Color.LIGHT_GRAY));
                    table.addCell(new Cell().add("" + orderItem.getQuantity()).setBackgroundColor(Color.LIGHT_GRAY));
                    table.addCell(new Cell().add("" + orderItem.getPhoneNumber()).setBackgroundColor(Color.LIGHT_GRAY));
                    table.addCell(new Cell().add("" + orderItem.getPayType()).setBackgroundColor(Color.LIGHT_GRAY));
                    table.addCell(new Cell().add("" + orderItem.getCommissionFee()).setBackgroundColor(Color.LIGHT_GRAY));
                    table.addCell(new Cell().add("" + orderItem.getTotalPriceWithCommissionFee()).setBackgroundColor(Color.LIGHT_GRAY));
                    table.addCell(new Cell().add("" + orderItem.getTimeDate()).setBackgroundColor(Color.LIGHT_GRAY));
                } else {
                    table.addCell(new Cell().add("" + (i++)).setBackgroundColor(Color.GRAY));
                    table.addCell(new Cell().add(orderItem.getFullName()).setBackgroundColor(Color.GRAY));
                    table.addCell(new Cell().add(orderItem.getBookName()).setBackgroundColor(Color.GRAY));
                    table.addCell(new Cell().add("" + orderItem.getPrice()).setBackgroundColor(Color.GRAY));
                    table.addCell(new Cell().add("" + orderItem.getQuantity()).setBackgroundColor(Color.GRAY));
                    table.addCell(new Cell().add("" + orderItem.getPhoneNumber()).setBackgroundColor(Color.GRAY));
                    table.addCell(new Cell().add("" + orderItem.getPayType()).setBackgroundColor(Color.GRAY));
                    table.addCell(new Cell().add("" + orderItem.getCommissionFee()).setBackgroundColor(Color.GRAY));
                    table.addCell(new Cell().add("" + orderItem.getTotalPriceWithCommissionFee()).setBackgroundColor(Color.GRAY));
                    table.addCell(new Cell().add("" + orderItem.getTimeDate()).setBackgroundColor(Color.GRAY));
                }

            }

            Double total = purchaseHistory.stream()
                    .map(item -> item.getTotalPriceWithCommissionFee())
                    .reduce(0.0, (a, b) -> a + b);

            table.addCell(new Cell().add(""));
            table.addCell(new Cell().add(""));
            table.addCell(new Cell().add(""));
            table.addCell(new Cell().add(""));
            table.addCell(new Cell().add(""));
            table.addCell(new Cell().add(""));
            table.addCell(new Cell().add(""));
            table.addCell(new Cell().add(""));
            table.addCell(new Cell().add("TOTAL PRICE " + total).setBackgroundColor(Color.RED));
            table.addCell(new Cell().add(""));

            document.add(table);

            document.close();
            file = new File("src/main/resources/purchaseHistory.pdf");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return file;
    }

}
