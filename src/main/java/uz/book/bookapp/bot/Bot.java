package uz.book.bookapp.bot;


//Asadbek Xalimjonov 28/03/22 17:26

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.CallbackQuery;
import org.telegram.telegrambots.meta.api.objects.InputFile;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import uz.book.bookapp.bot.basedButtonService.UserButton;
import uz.book.bookapp.entity.book.Book;
import uz.book.bookapp.entity.payment.PayType;
import uz.book.bookapp.entity.user.User;
import uz.book.bookapp.entity.user.UserActivity;
import uz.book.bookapp.projection.CustomCart;
import uz.book.bookapp.repository.BookRepo;
import uz.book.bookapp.repository.PayTypeRepo;
import uz.book.bookapp.service.CartServiceImpl;
import uz.book.bookapp.service.PurchaseHistoryServiceImpl;
import uz.book.bookapp.service.UserServiceImpl;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
public class Bot extends TelegramLongPollingBot {

    public Map<Long, UserActivity> userActivityMap = new HashMap<>();

    @Autowired
    private UserServiceImpl userService;

    @Autowired
    private BookRepo bookRepo;

    @Autowired
    private PayTypeRepo payTypeRepo;

    @Autowired
    private CartServiceImpl cartService;

    @Autowired
    private PDFGenerator pdfGenerator;

    @Autowired
    private PurchaseHistoryServiceImpl purchaseHistoryService;

    @Autowired
    private UserButton userButton;

    @Value("${bot_username}")
    String username;

    @Value("${bot_token}")
    String token;

    @Override
    public String getBotUsername() {

        return username;
    }

    @Override
    public String getBotToken() {
        return token;
    }

    @Override
    public void onUpdateReceived(Update update) {

        if (update.hasMessage()) {
            updateHasMessage(update);
        } else if (update.hasCallbackQuery()) {
            callBackQueryMethod(update);
        }

    }

    private void updateHasMessage(Update update) {
        Long chatId = update.getMessage().getChatId();
        User currentUser = userService.findByIdChatId(chatId);
        UserActivity currentUserActivity;
        currentUserActivity = getCurrentUserActivity(chatId);

        if (currentUser == null || currentUserActivity == null) return;
        currentUserActivity.setUser(currentUser);

        Message message = update.getMessage();
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId.toString());

        if (!message.hasContact() && message.getText().equalsIgnoreCase("Menu")) {
            sendMessage.setText("Menu");
            sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
            currentUserActivity.setRound(1);
        }

        switch (currentUserActivity.getRound()) {
            case 0:
                if (message.getText().equalsIgnoreCase("/start")) {
                    sendMessage.setText("WELCOME BOOK BOT");
                    sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                    currentUserActivity.setRound(1);
                } else {
                    sendMessage.setText("WELCOME BOOK BOT");
                    sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                    currentUserActivity.setRound(1);
                    break;
                }
                break;
            case 1:
                if (message.hasContact()) {
                    currentUser = userService.updateUser(currentUser, message.getContact());
                    sendMessage.setText("Menu");
                    sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                    currentUserActivity.setRound(2);
                }
                break;
            case 2:
                switch (message.getText()) {
                    case "\uD83D\uDCD4 Book List":
                        sendMessage.setText("Books");
                        sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                        currentUserActivity.setRound(3);
                        break;
                    case "\uD83D\uDED2 My Cart":
                        List<CustomCart> customCarts = userService.showMyCart(currentUserActivity);
                        String cart = "My Cart\n";
                        if (customCarts.size() < 1) cart += "Cart is empty";

                        for (CustomCart customCart : customCarts) {
                            cart += "Book  : " + customCart.getBookName() + "\nQuantity : " + customCart.getQuantity() + "\n";
                        }
                        sendMessage.setText(cart);
                        currentUserActivity.setRound(1);
                        sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                        currentUserActivity.setRound(2);
                        break;
                    case "\uD83D\uDCB3 Chekout":
                        if (userService.showMyCart(currentUserActivity).size() > 0) {
                            sendMessage.setText("Choose paytype");
                            currentUserActivity.setRound(6);
                            sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                        } else {
                            sendMessage.setText("Cart empty");
                            currentUserActivity.setRound(1);
                            sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                            currentUserActivity.setRound(2);
                        }
                        break;
                    case "\uD83D\uDCD1 Purchase History":
                        if (purchaseHistoryService.getPurchaseHistory(currentUser.getId()).size() > 1) {

                            File purchaseHistory = pdfGenerator.getPurchaseHistory(currentUser.getFullName(), purchaseHistoryService.getPurchaseHistory(currentUser.getId()));
                            SendDocument sendDocument = new SendDocument();
                            sendDocument.setChatId(chatId.toString());
                            InputFile inputFile = new InputFile(purchaseHistory);
                            sendDocument.setDocument(inputFile);
                            sendDocument.setCaption("Purchase History");
                            currentUserActivity.setRound(1);
                            sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                            currentUserActivity.setRound(2);
                            sendMessage.setText("Document");
                            try {
                                execute(sendDocument);
                            } catch (TelegramApiException e) {
                                e.printStackTrace();
                            }
                        } else {
                            sendMessage.setText("No Purchase History");
                            currentUserActivity.setRound(1);
                            sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                            currentUserActivity.setRound(2);
                        }
                        break;
                    case "\uD83D\uDCB5 Fill Balance":
                        sendMessage.setText("Balance : " + currentUser.getFullName().getBalance() + "\nEnter amount");
                        currentUserActivity.setRound(5);
                        break;
                    default:
                        return;
                }
                break;
            case 5: {
                try {
                    double amount = Double.parseDouble(message.getText());
                    if (amount > 0) {
                        sendMessage.setText("Successfully Added");
                        userService.addBalance(currentUser.getId(), amount);
                    } else {
                        sendMessage.setText("Invalid amount! Please try again");
                    }
                } catch (Exception e) {
                    sendMessage.setText("Please enter number");
                }
                currentUserActivity.setRound(1);
                sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                currentUserActivity.setRound(2);
            }
            case 7: {
                if (message.getText().equals("\uD83D\uDCB4 Purchase All")) {
                    boolean result = cartService.buyAll(currentUserActivity);
                    if (result) sendMessage.setText("Successfully bought");
                    else sendMessage.setText("Please check your balance");
                    currentUserActivity.setRound(1);
                    sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                    currentUserActivity.setRound(2);

                } else if (message.getText().equals("\uD83D\uDEE2 Clear Cart")) {
                    sendMessage.setText("Cleared");
                    userService.clearCart(currentUser.getId());
                    currentUserActivity.setRound(1);
                    sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                    currentUserActivity.setRound(2);
                } else if (message.getText().equals("\uD83E\uDDE8 Remove item")) {
                    sendMessage.setText("Choose a book");
                    currentUserActivity.setRound(8);
                    sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                }
            }
            break;
            default:
                sendMessage.setText("Menu");
                currentUserActivity.setRound(0);
                sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                currentUserActivity.setRound(1);
                break;

        }

        sendMessage.setChatId(chatId.toString());

        try {

            execute(sendMessage);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }

    private void callBackQueryMethod(Update update) {
        CallbackQuery callbackQuery = update.getCallbackQuery();
        Long chatId = callbackQuery.getMessage().getChatId();
        User currentUser = userService.findByIdChatId(chatId);
        UserActivity currentUserActivity;
        currentUserActivity = getCurrentUserActivity(chatId);

        if (currentUser == null || currentUserActivity == null) return;
        currentUserActivity.setUser(currentUser);

        SendMessage sendMessage = new SendMessage();
        EditMessageText editMessageText = new EditMessageText();

        sendMessage.setChatId(chatId.toString());
        editMessageText.setChatId(chatId.toString());

        DeleteMessage deleteMessage = new DeleteMessage();
        deleteMessage.setChatId(chatId.toString());

        String data = callbackQuery.getData();

        if (data.equalsIgnoreCase("Menu")) {
            sendMessage.setText("Menu");
            currentUserActivity.setRound(1);
            sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
            currentUserActivity.setRound(2);
            try {
                deleteMessage.setMessageId(callbackQuery.getMessage().getMessageId());
                execute(deleteMessage);
                execute(sendMessage);
                return;
            } catch (TelegramApiException e) {
                e.printStackTrace();
            }
        }


        switch (currentUserActivity.getRound()) {
            case 3: {
                if (bookRepo.findById(data).isPresent()) {

                    Book book1 = bookRepo.findById(data).get();
                    String authors = "";

                    String book = "Title : " + book1.getTitle() + "\nDescription : " + book1.getDescription() + "\nPrice : " + book1.getPrice() + "\nAuthors : " + authors;
                    sendMessage.setText(book);
                    currentUserActivity.setRound(4);
                    currentUserActivity.setSelectedBook(book1);
                    sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));

                    try {

                        execute(sendMessage);
                        return;

                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
                break;
            }
            case 4: {
                if (data.equals("plus")) {
                    currentUserActivity.setCurrentQuantity(currentUserActivity.getCurrentQuantity() + 1);
                } else if (data.equals("minus")) {
                    currentUserActivity.setCurrentQuantity(currentUserActivity.getCurrentQuantity() - 1);
                }

                Book book1 = currentUserActivity.getSelectedBook();
                String authors = "";

                String book = "Title : " + book1.getTitle() + "\nDescription : " + book1.getDescription() + "\nPrice : " + book1.getPrice() + "\nAuthors : " + authors;
                editMessageText.setText(book);
                currentUserActivity.setRound(4);
                currentUserActivity.setSelectedBook(book1);
                editMessageText.setReplyMarkup((InlineKeyboardMarkup) userButton.getReplyKeyBoardMarkup(currentUserActivity));

            }
            case 6: {

                if (payTypeRepo.findById(data).isPresent()) {
                    PayType payType = payTypeRepo.findById(data).get();
                    currentUserActivity.setRound(7);
                    currentUserActivity.setPayType(payType);
                    String total = "";
                    List<CustomCart> customCarts = userService.showMyCart(currentUserActivity);
                    Double sum = customCarts.stream().map(item -> item.getPrice() * item.getQuantity()).reduce(0.0, (a, b) -> a + b);

                    String cart = "My Cart\n";

                    for (CustomCart customCart : customCarts) {
                        cart += "Book  : " + customCart.getBookName() + "\nQuantity : " + customCart.getQuantity() + "\nPrice : " + customCart.getPrice() + "\n";
                    }

                    cart += "\nTotalPrice : " + sum;
                    cart += "\nTotalPrice With CommssionFee : " + (sum * (1 + payType.getCommissionFee() / 100));

                    sendMessage.setText(cart);
                    sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));

                    try {
                        deleteMessage.setMessageId(callbackQuery.getMessage().getMessageId());
                        execute(deleteMessage);
                        execute(sendMessage);
                        return;
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }
                }
            }
            case 8:{
                CustomCart customCart1 = userService.showMyCart(currentUserActivity).stream().filter(customCart -> customCart.getBookId().equals(data))
                        .findFirst()
                        .orElse(null);
                if (customCart1!=null)
                {
                    Book book = bookRepo.findById(customCart1.getBookId()).get();
                    currentUserActivity.setSelectedBook(book);
                    userService.removeBook(currentUserActivity,customCart1);
                    sendMessage.setText("Success fully removed");
                    currentUserActivity.setRound(1);
                    sendMessage.setReplyMarkup(userButton.getReplyKeyBoardMarkup(currentUserActivity));
                    currentUserActivity.setRound(2);
                    try {
                        deleteMessage.setMessageId(callbackQuery.getMessage().getMessageId());
                        execute(deleteMessage);
                        execute(sendMessage);
                        return;
                    } catch (TelegramApiException e) {
                        e.printStackTrace();
                    }

                }
            }
            break;
        }


        if (data.equals("addtoCart")) {
            boolean result = userService.addToCart(currentUserActivity);
            if (result) {

                deleteMessage.setMessageId(callbackQuery.getMessage().getMessageId());
                currentUserActivity.setCurrentQuantity(1);
                currentUserActivity.setSelectedBook(null);
                sendMessage.setText("Successfully added to cart");
                currentUserActivity.setRound(3);
                try {
                    execute(deleteMessage);
                    Message execute = execute(sendMessage);
                    execute.getMessageId();
                    Thread thread = new Thread(() -> {
                        for (int i = 3; i > 0; i--) {
                            try {
                                Thread.sleep(1000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                        }

                        deleteMessage.setMessageId(execute.getMessageId());
                        try {
                            execute(deleteMessage);
                        } catch (TelegramApiException e) {
                            e.printStackTrace();
                        }

                    });
                    thread.start();

                    return;
                } catch (TelegramApiException e) {
                    e.printStackTrace();
                }
            }
        }

        editMessageText.setMessageId(callbackQuery.getMessage().getMessageId());

        try {
            execute(editMessageText);
        } catch (TelegramApiException e) {
            e.printStackTrace();
        }
    }


    private UserActivity getCurrentUserActivity(Long chatId) {
        UserActivity userActivity;
        if (userActivityMap.containsKey(chatId)) {
            userActivity = userActivityMap.get(chatId);
        } else {
            userActivityMap.put(chatId, new UserActivity(chatId));
            userActivity = userActivityMap.get(chatId);
        }
        return userActivity;
    }


}
