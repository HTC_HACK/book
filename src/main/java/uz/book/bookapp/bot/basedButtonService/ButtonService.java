package uz.book.bookapp.bot.basedButtonService;

import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import uz.book.bookapp.entity.user.User;
import uz.book.bookapp.entity.user.UserActivity;

public interface ButtonService {
    ReplyKeyboard getReplyKeyBoardMarkup(UserActivity currentUser);

    ReplyKeyboard getInlineKeyBoardMarkup(UserActivity currentUser);
}
