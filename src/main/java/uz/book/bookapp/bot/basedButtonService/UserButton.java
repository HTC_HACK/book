package uz.book.bookapp.bot.basedButtonService;


//Asadbek Xalimjonov 28/03/22 17:38


import org.checkerframework.checker.units.qual.A;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import uz.book.bookapp.entity.book.Book;
import uz.book.bookapp.entity.payment.PayType;
import uz.book.bookapp.entity.user.User;
import uz.book.bookapp.entity.user.UserActivity;
import uz.book.bookapp.projection.CustomCart;
import uz.book.bookapp.repository.BookRepo;
import uz.book.bookapp.repository.PayTypeRepo;
import uz.book.bookapp.service.UserServiceImpl;

import java.util.ArrayList;
import java.util.List;

@Component
public class UserButton implements ButtonService {

    @Autowired
    private BookRepo bookRepo;

    @Autowired
    private PayTypeRepo payTypeRepo;

    @Autowired
    private UserServiceImpl userService;

    @Override
    public ReplyKeyboard getReplyKeyBoardMarkup(UserActivity currentUser) {
        //INLINE
        InlineKeyboardMarkup inlineKeyboardMarkup = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> inlineButtons = new ArrayList<>();
        List<InlineKeyboardButton> buttonList = new ArrayList<>();
        inlineButtons.add(buttonList);
        inlineKeyboardMarkup.setKeyboard(inlineButtons);


        ReplyKeyboardMarkup keyboardMarkup = new ReplyKeyboardMarkup();
        List<KeyboardRow> rowList = new ArrayList<>();
        keyboardMarkup.setKeyboard(rowList);
        keyboardMarkup.setResizeKeyboard(true);
        keyboardMarkup.setOneTimeKeyboard(true);

        switch (currentUser.getRound()) {
            case 0: {
                KeyboardRow row1 = new KeyboardRow();
                KeyboardButton shareButton = new KeyboardButton();
                shareButton.setText("\uD83D\uDCF2 Share contact");
                shareButton.setRequestContact(true);
                row1.add(shareButton);
                rowList.add(row1);
            }
            break;
            case 1: {
                KeyboardRow row1 = new KeyboardRow();
                row1.add("\uD83D\uDCD4 Book List");
                row1.add("\uD83D\uDED2 My Cart");
                KeyboardRow row2 = new KeyboardRow();
                row2.add("\uD83D\uDCB3 Chekout");
                row2.add("\uD83D\uDCD1 Purchase History");
                KeyboardRow row3 = new KeyboardRow();
                row3.add("\uD83D\uDCB5 Fill Balance");
                rowList.add(row1);
                rowList.add(row2);
                rowList.add(row3);
            }
            break;
            case 2: {
                List<InlineKeyboardButton> inlineRow = new ArrayList<>();
                List<InlineKeyboardButton> inlinePrev = new ArrayList<>();
                List<Book> bookRepoAll = bookRepo.findAll();
                for (int i = 0; i < bookRepoAll.size(); i++) {
                    Book value = bookRepoAll.get(i);

                    InlineKeyboardButton buttonN = new InlineKeyboardButton();
                    buttonN.setText(value.getTitle());
                    buttonN.setCallbackData("" + value.getId());
                    inlineRow.add(buttonN);

                    if (i % 2 == 0) {
                        inlineButtons.add(inlineRow);
                    } else {
                        inlineRow = new ArrayList<>();
                    }
                }
                InlineKeyboardButton buttonH = new InlineKeyboardButton();
                buttonH.setText("⏏️Menu");
                buttonH.setCallbackData("Menu");
                inlinePrev.add(buttonH);

                inlineButtons.add(inlinePrev);
                return inlineKeyboardMarkup;
            }
            case 4: {
                List<InlineKeyboardButton> inlinePrev = new ArrayList<>();
                List<InlineKeyboardButton> inlinePrev2 = new ArrayList<>();

                if (currentUser.getCurrentQuantity() > 1) {
                    InlineKeyboardButton buttonH = new InlineKeyboardButton();
                    buttonH.setText("-");
                    buttonH.setCallbackData("minus");
                    inlinePrev.add(buttonH);
                }

                InlineKeyboardButton buttonM = new InlineKeyboardButton();
                buttonM.setText("" + currentUser.getCurrentQuantity());
                buttonM.setCallbackData("qauntity");
                inlinePrev.add(buttonM);

                InlineKeyboardButton buttonL = new InlineKeyboardButton();
                buttonL.setText("+");
                buttonL.setCallbackData("plus");
                inlinePrev.add(buttonL);

                InlineKeyboardButton buttonLH = new InlineKeyboardButton();
                buttonLH.setText("Add to Cart");
                buttonLH.setCallbackData("addtoCart");
                inlinePrev2.add(buttonLH);

                inlineButtons.add(inlinePrev);
                inlineButtons.add(inlinePrev2);
                return inlineKeyboardMarkup;
            }
            case 6: {
                List<InlineKeyboardButton> inlineRow = new ArrayList<>();
                List<InlineKeyboardButton> inlinePrev = new ArrayList<>();
                List<PayType> payTypes = payTypeRepo.findAll();
                for (int i = 0; i < payTypes.size(); i++) {
                    PayType value = payTypes.get(i);

                    InlineKeyboardButton buttonN = new InlineKeyboardButton();
                    buttonN.setText(value.getName());
                    buttonN.setCallbackData("" + value.getId());
                    inlineRow.add(buttonN);

                    if (i % 2 == 0) {
                        inlineButtons.add(inlineRow);
                    } else {
                        inlineRow = new ArrayList<>();
                    }
                }

                InlineKeyboardButton buttonH = new InlineKeyboardButton();
                buttonH.setText("⏏️Menu");
                buttonH.setCallbackData("Menu");
                inlinePrev.add(buttonH);

                inlineButtons.add(inlinePrev);
                return inlineKeyboardMarkup;
            }
            case 7: {
                KeyboardRow row1 = new KeyboardRow();
                row1.add("\uD83D\uDCB4 Purchase All");
                row1.add("\uD83D\uDEE2 Clear Cart");
                KeyboardRow row2 = new KeyboardRow();
                row2.add("\uD83E\uDDE8 Remove item");
                rowList.add(row1);
                rowList.add(row2);
            }
            break;
            case 8: {
                List<InlineKeyboardButton> inlineRow = new ArrayList<>();
                List<InlineKeyboardButton> inlinePrev = new ArrayList<>();
                List<CustomCart> customCarts = userService.showMyCart(currentUser);
                for (int i = 0; i < customCarts.size(); i++) {
                    CustomCart value = customCarts.get(i);

                    InlineKeyboardButton buttonN = new InlineKeyboardButton();
                    buttonN.setText(value.getBookName());
                    buttonN.setCallbackData("" + value.getBookId());
                    inlineRow.add(buttonN);

                    if (i % 2 == 0) {
                        inlineButtons.add(inlineRow);
                    } else {
                        inlineRow = new ArrayList<>();
                    }
                }
                InlineKeyboardButton buttonH = new InlineKeyboardButton();
                buttonH.setText("⏏️Menu");
                buttonH.setCallbackData("Menu");
                inlinePrev.add(buttonH);

                inlineButtons.add(inlinePrev);
                return inlineKeyboardMarkup;
            }
            default:
                KeyboardRow row4 = new KeyboardRow();
                row4.add("◀️ Back");
                row4.add("⏏️ Menu");
                rowList.add(row4);
                break;
        }

        return keyboardMarkup;

    }

    @Override
    public ReplyKeyboard getInlineKeyBoardMarkup(UserActivity currentUser) {
        return null;
    }
}
