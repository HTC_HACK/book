package uz.book.bookapp.entity.payment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.book.bookapp.entity.absEntity.AbsEntity;
import uz.book.bookapp.entity.book.Book;
import uz.book.bookapp.entity.user.User;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 28/03/22 16:30

@Entity(name = "carts")
public class Cart extends AbsEntity {

    @ManyToOne
    private User user;

    @ManyToOne
    private Book book;

    private int quantity;


}
