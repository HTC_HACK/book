package uz.book.bookapp.entity.payment;

import jdk.jfr.Enabled;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.book.bookapp.entity.absEntity.AbsEntity;

import javax.persistence.Entity;
import java.sql.Timestamp;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 28/03/22 16:31

@Entity(name = "payments")
public class PayType extends AbsEntity {

    private String name;
    private Double commissionFee;

}
