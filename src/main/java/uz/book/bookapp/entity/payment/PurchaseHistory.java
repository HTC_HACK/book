package uz.book.bookapp.entity.payment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.book.bookapp.entity.absEntity.AbsEntity;
import uz.book.bookapp.entity.book.Book;
import uz.book.bookapp.entity.user.User;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 28/03/22 16:32

@Entity(name = "purchase_history")
public class PurchaseHistory extends AbsEntity {

    @ManyToOne
    private User user;

    @ManyToOne
    private Book book;

    @ManyToOne
    private PayType payType;

    private Integer quantity;

    private Double totalPriceWithCommissionFee;


}
