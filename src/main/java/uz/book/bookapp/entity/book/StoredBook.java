package uz.book.bookapp.entity.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.book.bookapp.entity.absEntity.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 28/03/22 18:25


@Entity(name = "stored_books")
public class StoredBook extends AbsEntity {

    @ManyToOne
    private Book book;

    private int quantity;
}
