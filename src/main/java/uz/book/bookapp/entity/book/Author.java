package uz.book.bookapp.entity.book;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.book.bookapp.entity.absEntity.AbsEntity;
import uz.book.bookapp.entity.user.UserDetails;

import javax.persistence.Embedded;
import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 28/03/22 16:20


@Entity(name = "authors")
public class Author extends AbsEntity {

    @Embedded
    private UserDetails userDetails;

}
