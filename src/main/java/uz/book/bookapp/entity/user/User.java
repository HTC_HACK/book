package uz.book.bookapp.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.book.bookapp.entity.absEntity.AbsEntity;

import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

//Asadbek Xalimjonov 28/03/22 16:15

@Entity(name = "users")
public class User extends AbsEntity {

    private String phoneNumber;
    private String username;
    private Long chatId;

    @Enumerated(EnumType.STRING)
    private Role role;

    private int round;

    @Embedded
    private UserDetails fullName;

}
