package uz.book.bookapp.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import uz.book.bookapp.entity.book.Book;
import uz.book.bookapp.entity.payment.PayType;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserActivity {
    private Long chatId;
    private int round;
    private User user;
    private int currentQuantity = 1;
    private Book selectedBook;
    private PayType payType;

    public UserActivity(Long chatId) {
        this.chatId = chatId;
    }
}
