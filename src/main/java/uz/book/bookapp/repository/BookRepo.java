package uz.book.bookapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.book.bookapp.entity.book.Book;

@Repository
public interface BookRepo extends JpaRepository<Book,String> {
}
