package uz.book.bookapp.repository;



//Asadbek Xalimjonov 28/03/22 16:39

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.book.bookapp.entity.user.User;

import javax.swing.text.html.Option;
import java.util.Optional;

@Repository
public interface UserRepo extends JpaRepository<User,String> {
}
