package uz.book.bookapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.book.bookapp.projection.CustomCart;
import uz.book.bookapp.entity.payment.Cart;

import java.util.List;

@Repository
public interface CartRepo extends JpaRepository<Cart, String> {

    @Query(nativeQuery = true,
            value = "select b.id as bookId,b.title as bookName,b.price as price,carts.quantity as quantity,carts.id as cartId \n" +
                    "from carts\n" +
                    "join books b on b.id=carts.book_id\n" +
                    "where user_id=:userId")
    List<CustomCart> getMyCart(@Param("userId") String userId);


}
