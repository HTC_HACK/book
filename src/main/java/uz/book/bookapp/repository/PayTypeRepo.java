package uz.book.bookapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.book.bookapp.entity.payment.PayType;

@Repository
public interface PayTypeRepo extends JpaRepository<PayType,String> {
}
