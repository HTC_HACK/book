package uz.book.bookapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.book.bookapp.entity.book.StoredBook;

public interface StoredBookRepo extends JpaRepository<StoredBook,String> {
}
