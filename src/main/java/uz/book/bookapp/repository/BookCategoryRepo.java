package uz.book.bookapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.book.bookapp.entity.book.BookCategory;

@Repository
public interface BookCategoryRepo extends JpaRepository<BookCategory,String> {
}
