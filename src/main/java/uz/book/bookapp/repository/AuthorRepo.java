package uz.book.bookapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.book.bookapp.entity.book.Author;

@Repository
public interface AuthorRepo extends JpaRepository<Author,String> {
}
