package uz.book.bookapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import uz.book.bookapp.projection.CustomPurchaseHistory;
import uz.book.bookapp.entity.payment.PurchaseHistory;

import java.util.List;

@Repository
public interface PurchaseHistoryRepo extends JpaRepository<PurchaseHistory, String> {

    @Query(nativeQuery = true,
            value = "select b.title as bookName,\n" +
                    "       purchase_history.quantity as quantity,\n" +
                    "       b.price as price,\n" +
                    "       concat(u.first_name,' ',u.last_name) as fullName,\n" +
                    "       u.phone_number as phoneNumber,\n" +
                    "       p.name as payType,\n" +
                    "       p.commission_fee as commissionFee,\n" +
                    "       purchase_history.total_price_with_commission_fee as totalPriceWithCommissionFee, " +
                    "       DATE(purchase_history.created_at) as timeDate \n" +
                    "from purchase_history\n" +
                    "join books b on b.id = purchase_history.book_id\n" +
                    "join users u on u.id=purchase_history.user_id\n" +
                    "join payments p on p.id = purchase_history.pay_type_id\n" +
                    "where u.id=:userId ")
    List<CustomPurchaseHistory> getPurchaseHistory(@Param("userId") String userId);
}
